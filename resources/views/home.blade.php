@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @if (isset($update))
            <form action="/save" method="POST">
            修改資料<br>
            <input type="text" hidden value="{{$update->id}}" name="id">
            Name ：<input type="text" value="{{$update->Name}}" name="Name">
            <br>
            Age  ：<input type="text" value="{{$update->age}}" name="age">
            <br>
            Sex  ：<input type="text" value="{{$update->sex}}" name="sex">
            <br>
            @csrf
            <input type="submit" value="更新">
        @else
            <form action="/add" method="POST">
            新增資料<br>
            Name ：<input type="text" value="" name="Name">
            <br>
            Age  ：<input type="text" value="" name="age">
            <br>
            Sex  ：<input type="text" value="" name="sex">
            <br>
            @csrf
            <input type="submit" value="送出">
        @endif
    </form>
    <br>
    <br>

    <div class="container">
        <div class="row justify-content-end">
          <div class="col-3"><span>顯示

            <select name="limit" id="limit" onchange="set_limit()";>
                @if (isset($_GET["limit"]))
                    <option value="" disabled selected hidden>{{$_GET["limit"]}}</option>
                )@endif
                <option value="3">3</option>
                <option value="5"
                @if (!isset($_GET["limit"]))
                    echo selected="selected"
                )@endif
                >5</option>
                <option value="8">8</option>
                <option value="10">10</option>
                <option value="20">20</option>
            </select>

            筆資料</span></div>
        </div>
    </div>
    <script>
        function set_limit() {
            location.replace('?limit=' + document.getElementById("limit").value);
            //alert(document.getElementById("limit").value);
        }
        </script>
    <br>
    {{-- <form action="/edit" method="POST"> --}}
    <div>
        <table width="500" align="center" class="table table-striped">
            <tr align="center">
                <td width="80">ID</td>
                <td width="80">Name</td>
                <td width="80">Age</td>
                <td width="80">Sex</td>
                <td width="180">Edit</td>
            </tr>

            @php ($a=$data->count()-1)

            @foreach ($data as $item)
            <tr align="center">

                <td>{{$data->lastItem() - $a--}}</td>
                <td>{{$item->Name}}</td>
                <td>{{$item->age}}</td>
                <td>{{$item->sex}}</td>
                <td>
                    <button class="btn btn-warning btn-sm"><a href="{{route('update')}}?id={{$item->id}}@if (isset($page))&page={{$page}}@endif">update</a> </button>
                    <button class="btn btn-danger btn-sm"><a href="{{route('delete')}}?id={{$item->id}}@if (isset($page))&page={{$page}}@endif">delete</a> </button>
                </td>
            </tr>
            @endforeach

            @csrf
        </table>
    </div>

    {{-- </form> --}}
    {{-- {{dd(Request::capture()->all())}} --}}
    {!! $data->appends(Request::capture()->except('page'))->render() !!}
    {{-- {{dd($data->appends(Request::capture()->except('page'))->render())}} --}}
    {{-- {{dd($data->appends(Request::capture()->except('page')))}} --}}
    {{-- {!! $data->links() !!} --}}
    {{-- {{dd($data->lastItem())}} --}}
        </div>
    </div>
</div>
@endsection
