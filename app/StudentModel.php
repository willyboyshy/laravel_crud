<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentModel extends Model
{
    protected $table = "demo0715";
    protected $fillable = ['Name', 'age', 'sex'];
    public $timestamps = false;
}
