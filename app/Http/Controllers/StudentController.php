<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\StudentModel;
use Illuminate\Contracts\View\View;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    //$re->input("name");
    public function list(Request $re)
    {
        $limit = isset($re->except("_token")["limit"]) ? ($re->except("_token")["limit"]) : 5;
        $g_data = StudentModel::orderBy("id")->Paginate($limit);
        if (isset($re->except("_token")["page"])) {
            $p = ($re->except("_token")["page"]);
            return view("home", ['data' => $g_data, 'page' => $p]);
        } else {
            return view("home", ['data' => $g_data]);
        }
    }
    public function add(Request $re)
    {
        StudentModel::insert($re->except("_token"));
        return redirect()->action('StudentController@list');
    }

    public function save(Request $re)
    {
        $data = $re->except("_token");
        $k = StudentModel::where('id', $data["id"])
            ->update([
                'Name' => $data["Name"],
                'age' => $data["age"],
                'sex' => $data["sex"],
            ]);
        $g_data = StudentModel::orderBy("id")->get();
        return redirect()->action('StudentController@list');
    }



    public function delete(Request $re)
    {
        StudentModel::destroy($re->except("_token")["id"]);
        return redirect()->action('StudentController@list');
    }

    public function getbyid(Request $re)
    {

        return StudentModel::where('id', $re->except("_token")["id"])->get();
    }

    public function update(Request $re)
    {
        $id = $re->except("_token")["id"];
        $g_data = StudentModel::orderBy("id")->Paginate(5);
        $up_data = StudentModel::where('id', $id)->get();
        if ($re->except("_token")["page"]) {
            $page = $re->except("_token")["page"];
        }
        return view("home", ['update' => $up_data[0], 'data' => $g_data]);
    }
}
