<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/postman', function (Request $re) {
    return dd($re->all());
    //return $re->all()[key($re->all())];
});

ROUTE::get('/delete', 'StudentController@delete')->name('delete');
ROUTE::get('/update', 'StudentController@update')->name('update');

Route::any('/add', 'StudentController@add');
Route::any('/save', 'StudentController@save');
Auth::routes();

Route::any('/home', 'StudentController@list');
